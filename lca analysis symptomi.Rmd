---
title: "Analysis of LCA"
author: "Bethany Doran"
date: "5/12/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#reading in database with the lca coding
hfaclass <- readRDS("~/hf-action/hfactionclassesxi.rds")

#install_bitbucket('hfclinicaldata/heart_analysis_utils')
library(heart.analysis.utils)
library(poLCA)
library(LCAvarsel)
library(tables)
library(stringr)
library(scales)

```

```{r merge data and class data}

#change new id to a character from a factor, delete old variable
hfaclass$id = as.character(hfaclass$newid)

hfaclass$newid<-NULL

```


## Making table 1

```{r table 1}

#dataframe with all the variables for table
hfatab<-with(hfaclass, data.frame(f.age, f.bmi, f.cabg, f.sex, f.sbpq, f.nyhaq, f.mi,
                                  f.ckd, f.copd, f.htn, f.diabetes, f.race, f.pci,
                                  f.vevco2t, f.rer, f.cpxdurq, f.peakvo2t, death,
                                  deathfu, hf, dhfu,kccqq, modal_class, modal_prob))

library(data.table)
library(base)

hfatab<-data.table(hfatab)
setkey(hfatab, modal_class)

aget <- as.data.frame(hfatab[, mean(age, na.rm = TRUE),by = modal_class])

meantable <- as.data.frame(hfatab[, j=list(
                                           mean_age=mean(age, na.rm = TRUE),
                                           mean_bmi=mean(bmi, na.rm = TRUE),
                                           mean_hr=mean(basehr, na.rm=TRUE), 
                                           mean_sbp=mean(sbp, na.rm=TRUE),
                                           mean_dbp=mean(dbp, na.rm=TRUE),
                                           mean_sixmwlkd=mean(sixmwlkd, na.rm=TRUE),
                                           mean_vevco2=mean(vevco2, na.rm=TRUE),
                                           mean_cpxdur=mean(cpxdur, na.rm=TRUE),
                                           mean_pvo2=mean(peakvo2, na.rm=TRUE),
                                           mean_lvef=mean(corelvef, na.rm=TRUE),
                                           mean_kccq=mean(KCCQOSB, na.rm=TRUE),
                                           mean_bnp=mean(bnp, na.rm=TRUE),
                                           mean_gfr=mean(egfr, na.rm=TRUE),
                                           mean_modal=mean(modal_prob, na.rm=TRUE),
                                           mean_hdl=mean(hdl, na.rm=TRUE),
                                           mean_ldl=mean(ldl, na.rm=TRUE)
                                           
                                           ),by = modal_class])

t(meantable)


#making the table for other factors - I haven't figured out how to make % tables yet

#dead
percdead<-table(hfatab$modal_class, hfatab$death)
addmargins(percdead)
40/258
30/395
115/553
#0.15, 0.07, 0.21

#hf hosp
perchosp<-table(hfatab$modal_class, hfatab$hf)
addmargins(perchosp)
146/258
173/395
299/553

#0.56, 0.44, 0.54

#diabetic
#percdm<-table(hfatab$modal_class, hfatab$diabetes)
#addmargins(percdm)

#0.29, 0.42, 0.18, 0.36; overall 0.32

#sex
percfemale<-table(hfatab$modal_class, hfatab$sex)
addmargins(percfemale)

123/630
112/528
119/418
245/554
599/2130

#1 - 0.20, 2-0.21, 3-0.28,3-0.44, overall - 0.28

#racec
percrace<-table(hfatab$modal_class, hfatab$racec)
addmargins(percrace)

#black
14/630
73/528
173/418
409/554
669/2130

#0.2, 0.13, 0.41, 0.74 (overall 0.31)

#white
560/630
435/528
218/418
109/554
1322/2130

#0.89, 0.82, 0.52, 0.20 (overall 0.62)

#cabg
perccabg<-table(hfatab$modal_class, hfatab$cabg)
addmargins(perccabg)

208/630
292/528
19/418
19/554
538/2130

#0.33, 0.55, 0.045, 0.034, (overall=0.25)

#pci
percpci<-table(hfatab$modal_class, hfatab$priorpci)
addmargins(percpci)
230/629
205/528
26/418
37/554
498/2129

#0.37, 0.39, 0.062, 0.067 (overall=0.23)

#treatment
perctx<-table(hfatab$modal_class, hfatab$trt_ab)
addmargins(perctx)

304/630
264/528
208/418
284/554
1060/2130

#0.48, 0.50, 0.49, 0.51

hfaclass$f.modal_class<-factor(hfaclass$modal_class)
hfaclass$death<-as.numeric(hfaclass$death)


hfacox<-coxph(Surv(hfaclass$deathfu, hfaclass$death) ~ factor(hfaclass$modal_class), hfaclass) 
hfaclass <- within(hfaclass, f.modal_class <- relevel(f.modal_class, ref = 4))
hfacox1<-coxph(Surv(hfaclass$deathfu, hfaclass$death) ~ factor(hfaclass$f.modal_class), hfaclass) 
hfacox1

hfacox2<-coxph(Surv(hfaclass$deathfu, hfaclass$death) ~ factor(hfaclass$modal_class)+hfaclass$f.age+hfaclass$f.sex+hfaclass$f.bmi+hfaclass$f.cabg
               +hfaclass$f.htn+hfaclass$f.diabetes+factor(hfaclass$f.race), hfaclass) 
hfaclass <- within(hfaclass, f.modal_class <- relevel(f.modal_class, ref = 3))
hfacox3<-coxph(Surv(hfaclass$deathfu, hfaclass$death) ~ factor(hfaclass$f.modal_class)+hfaclass$f.age+hfaclass$f.sex+hfaclass$f.bmi+hfaclass$f.cabg
               +hfaclass$f.htn+hfaclass$f.diabetes+factor(hfaclass$f.race)+factor(hfaclass$f.vevco2t)+factor(hfaclass$f.cpxdurq)+factor(hfaclass$f.peakvo2t)+factor(hfaclass$kccqq), hfaclass) 
hfacox3

#symptoms and hf hosps

hfacoxhf<-coxph(Surv(hfatab$deathfu, hfatab$hf) ~ factor(hfatab$modal_class), hfatab) 
hfatab <- within(hfatab, f.modal_class <- relevel(f.modal_class, ref = 3))
hfacox1hf<-coxph(Surv(hfatab$deathfu, hfatab$hf) ~ factor(hfatab$f.modal_class), hfatab) 
hfacox1hf

hfacox2hf<-coxph(Surv(hfatab$deathfu, hfatab$hf) ~ factor(hfatab$modal_class)+hfatab$f.age+hfatab$f.sex+hfatab$f.bmi+hfatab$f.cabg
               +hfatab$f.htn+hfatab$f.diabetes+factor(hfatab$f.race), hfatab) 
hfatab <- within(hfatab, f.modal_class <- relevel(f.modal_class, ref = 3))
hfacox3hf<-coxph(Surv(hfatab$deathfu, hfatab$hf) ~ factor(hfatab$f.modal_class)+hfatab$f.age+hfatab$f.sex+hfatab$f.bmi+hfatab$f.cabg
               +hfatab$f.htn+hfatab$f.diabetes+factor(hfatab$f.race), hfatab) 
hfacox3hf

"f.vevco2t", "f.rer", "f.cpxdurq", "f.peakvo2t", "kccqq"

```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
