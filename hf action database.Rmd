---
title: "hf action database"
author: "Bethany Doran"
date: "5/6/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#rm(list = ls())

library(haven)
library(knitr)
library(dataMaid)
library(magrittr)
library(Hmisc)
library(rpart)

#read in data
hfaction <- read_sas("~/Dropbox/BioLINCC files/HF_ACTION_2015a/Data_DR/analysis.sas7bdat")
View(hfaction)

#save data
#write.csv(hfaction, file = "hfaction.csv")
#saveRDS(hfaction, file = "hfactionr.rds")

#list variables - ls(hfaction)

#make codebook for variables - makeCodebook(hfaction, replace="TRUE")

#keep important variables
#?pro BNP missing 2k or so?

hfdat<-with(hfaction, data.frame(age, bmi, creatnin, diabetes, sex, basehr, sbp, dbp,
                  nyhacl, priormi, PVD, betab, stroke, hospsxmo,
                  sixmwlka, sixmwlkd, vevco2, cpxdur,
                  peakrer, peakvo2, corelvef, racec,
                  insulin, smoking, creatnc, bun,
                  beckb, totcholc, hdl, ldl, hema1cc,
                  KCCQTSB, KCCQQOLB, KCCQOSB, KCCQCSB,
                  EDUCAT, income, hyperten,
                  nyhaclbn, cabg, priorpci, 
                  revasc, COPD, diabetes, depress,
                  etiology, bnp, probnp, walkwork,
                  walkmet, egfr, dthhosp,
                  dhfu, cechfcfu, cechfcmp, peakvo3,
                  peakvo12, death, deathfu, newid, trt_ab))

summary(hfdat)

#recoding variables

#age
hfdat$ageq[hfdat$age<50]<-0
hfdat$ageq[hfdat$age>=50 & hfdat$age<60]<-1
hfdat$ageq[hfdat$age>=60 & hfdat$age<70]<-2
hfdat$ageq[hfdat$age>=70]<-3

hfdat$f.age<-factor(hfdat$ageq)

#bmi
hfdat$bmiq[hfdat$bmi<25]<-0
hfdat$bmiq[hfdat$bmi>=25 & hfdat$bmi<30]<-1
hfdat$bmiq[hfdat$bmi>=30 & hfdat$bmi<40]<-2
hfdat$bmiq[hfdat$bmi>=40]<-3

hfdat$f.bmi<-factor(hfdat$bmiq)

#hr
hfdat$hrq<-NA
hfdat$hrq[hfdat$basehr<60]<-0
hfdat$hrq[hfdat$basehr>=60 & hfdat$basehr <90]<-1
hfdat$hrq[hfdat$basehr>=90]<-2

hfdat$f.hrq<-factor(hfdat$hrq)

#sex
hfdat$f.sex<-factor(hfdat$sex)

#sbp
hfdat$sbpq[hfdat$sbp<100]<-0
hfdat$sbpq[hfdat$sbp>=100 & hfdat$sbp<120]<-1
hfdat$sbpq[hfdat$sbp>=120 & hfdat$sbp<140]<-2
hfdat$sbpq[hfdat$sbp>=140]<-3

hfdat$f.sbpq<-factor(hfdat$sbpq)

#nyha class
hfdat$nyhaq[hfdat$nyhacl=="2"]<-0
hfdat$nyhaq[hfdat$nyhacl=="3"]<-1
hfdat$nyhaq[hfdat$nyhacl=="4"]<-2

hfdat$f.nyhaq<-factor(hfdat$nyhaq)

#prior mi
hfdat$f.mi<-factor(hfdat$priormi)

#pvd
hfdat$f.pvd<-factor(hfdat$PVD)

#stroke
hfdat$f.stroke<-factor(hfdat$stroke)

#hospitalizations in the last 6 months
hfdat$hosps[hfdat$hospsxmo==0]<-0
hfdat$hosps[hfdat$hospsxmo==1]<-1
hfdat$hosps[hfdat$hospsxmo>=2]<-2

hfdat$f.hosps<-factor(hfdat$hosps)

#six minute walk
hfdat$sixmwlkdq[hfdat$sixmwlkd<=300]<-3
hfdat$sixmwlkdq[hfdat$sixmwlkd>300 & hfdat$sixmwlka<=372]<-2
hfdat$sixmwlkdq[hfdat$sixmwlkd>372 & hfdat$sixmwlkd<=436]<-1
hfdat$sixmwlkdq[hfdat$sixmwlkd>436]<-0

hfdat$f.sixmwlkdq<-factor(hfdat$sixmwlkdq)

#peakvo2 - beta blocker vs. non beta blocker ?will have to look at how cpex was done to determine if they were on one vs. not
#if on b-blocker; <12 mL/kg/min pred
hfdat$peakvo2t[hfdat$peakvo2>=20]<-0
hfdat$peakvo2t[hfdat$peakvo2<20 & hfdat$peakvo2>=14]<-1
hfdat$peakvo2t[hfdat$peakvo2<14]<-2

hfdat$f.peakvo2t<-factor(hfdat$peakvo2t)

#vevco2
hfdat$vevco2t[hfdat$vevco2<30]<-0
hfdat$vevco2t[hfdat$vevco2>=30 & hfdat$vevco2<36]<-1
hfdat$vevco2t[hfdat$vevco2>=36]<-2
label(hfdat$vevco2t)<-"vevco2; <30=0, >=30-36=1, >=36=2"

hfdat$f.vevco2t <-factor(hfdat$vevco2t)

#cpx duration by quartile
hfdat$cpxdurq[hfdat$cpxdur<7]<-3
hfdat$cpxdurq[hfdat$cpxdur>=7 & hfdat$cpxdur<9.7]<-2
hfdat$cpxdurq[hfdat$cpxdur>=9.7 & hfdat$cpxdur<12]<-1
hfdat$cpxdurq[hfdat$cpxdur>=12]<-0
label(hfdat$cpxdurq)<-"cpx duration by quartile"

hfdat$f.cpxdurq<-factor(hfdat$cpxdurq)

#adequate rer >=1.1
hfdat$rer[hfdat$peakrer>=1.1]<-0
hfdat$rer[hfdat$peakrer<1.1]<-1
label(hfdat$rer)<-"peak RER reached adequate (>=1.1)"

hfdat$f.rer<-factor(hfdat$rer)

#lvef
hfdat$lvef[hfdat$corelvef>30]<-0
hfdat$lvef[hfdat$corelvef<=30 & hfdat$corelvef>20]<-1
hfdat$lvef[hfdat$corelvef<=20]<-2

hfdat$f.lvef=factor(hfdat$lvef)
label(hfdat$lvef)<-"EF; >30=0, 20-30=1, <20=2"

#race 1=black, 2=white, 3=asian, 4=?other?
hfdat$f.race<-factor(hfdat$racec)

#smoking ?ask about smoking status
hfdat$f.smoke<-factor(hfdat$smoking)

#bun
hfdat$bun1[hfdat$bun<15]<-0
hfdat$bun1[hfdat$bun>=15 & hfdat$bun<20.5]<-1
hfdat$bun1[hfdat$bun>=20.5 & hfdat$bun<28]<-2
hfdat$bun1[hfdat$bun>=28]<-3

hfdat$f.bun1<-factor(hfdat$bun1)

#totchol
hfdat$chol[hfdat$totcholc>=220]<-1
hfdat$chol[hfdat$totcholc<220]<-0

hfdat$f.chol<-factor(hfdat$chol)

#hdl
hfdat$hdl1[(hfdat$hdl>=40 & hfdat$sex==1) |(hfdat$hdl>=50 & hfdat$sex==2)]<-0
hfdat$hdl1[(hfdat$hdl<40 & hfdat$sex==1) |(hfdat$hdl<50 & hfdat$sex==2)]<-1

hfdat$f.hdl1<-factor(hfdat$hdl1)

#ldl
hfdat$ldl1[(hfdat$ldl<70)]<-0
hfdat$ldl1[hfdat$ldl>=70 & hfdat$ldl<100]<-1
hfdat$ldl1[hfdat$ldl>=100 & hfdat$ldl<130]<-2
hfdat$ldl1[hfdat$ldl>=130]<-3

hfdat$f.ldl1<-factor(hfdat$ldl1)

#diabetes
hfdat$f.diabetes<-factor(hfdat$diabetes)

#beck 
hfdat$beck[hfdat$beckb<11]<-0
hfdat$beck[hfdat$beckb>=11 & hfdat$beckb<17]<-1
hfdat$beck[hfdat$beckb>=17 & hfdat$beckb<21]<-2
hfdat$beck[hfdat$beckb>=21 & hfdat$beckb<31]<-3
hfdat$beck[hfdat$beckb>=31]<-4

label(hfdat$beck)<-"BDI, 0=normal, 1=mild, 2=borderline,3=moderate, 4=severe"

hfdat$f.beck<-factor(hfdat$beck)

#KS total symptom score
hfdat$kcs[hfdat$KCCQOSB<25]<-3
hfdat$kcs[hfdat$KCCQOSB>=25 & hfdat$KCCQOSB<50]<-2
hfdat$kcs[hfdat$KCCQOSB>=50 & hfdat$KCCQOSB<75]<-1
hfdat$kcs[hfdat$KCCQOSB>=75]<-0

label(hfdat$kcs)<-"KCCQ Combined, 3=severe, 2=mod, 1=mild, 0=minimal"

hfdat$f.kcs<-factor(hfdat$kcs)

#education level
hfdat$f.edu<-factor(hfdat$EDUCAT)

#income
hfdat$f.income<-factor(hfdat$income)

#htn
hfdat$f.htn<-factor(hfdat$hyperten)

#nyha class
hfdat$nyha[hfdat$nyhacl==2]<-0
hfdat$nyha[hfdat$nyhacl==3 | hfdat$nyhacl==4]<-1

#cabg
hfdat$f.cabg<-factor(hfdat$cabg)

#pci
hfdat$f.pci<-factor(hfdat$priorpci)

#revasc
hfdat$f.revasc<-factor(hfdat$revasc)

#copd
hfdat$f.copd<-factor(hfdat$COPD)

#depression
hfdat$f.depression<-factor(hfdat$depress)

#etiology
hfdat$f.etiology<-factor(hfdat$etiology)
label(hfdat$f.etiology)<-"etiology of HF; ischemic, NICM"

#bnp NA's about 1368

#walk met (by quartile in study)
hfdat$met[hfdat$walkmet>3.1]<-0
hfdat$met[hfdat$walkmet<=3.1 & hfdat$walkmet>=2.8]<-1
hfdat$met[hfdat$walkmet<2.8 & hfdat$walkmet>=2.43]<-2
hfdat$met[hfdat$walkmet<2.43]<-3

hfdat$f.met<-factor(hfdat$met)

#gfr
hfdat$ckd[hfdat$egfr>60]<-0
hfdat$ckd[hfdat$egfr<=60 & hfdat$egfr>45]<-1
hfdat$ckd[hfdat$egfr<=45 & hfdat$egfr>=30]<-2
hfdat$ckd[hfdat$egfr<30]<-3

hfdat$f.ckd<-factor(hfdat$ckd)

#death
hfdat$f.death<-factor(hfdat$death)

#death/hf
hfdat$f.dthhf<-factor(hfdat$dthhosp)

#treatment arm
hfdat$f.trtarm<-factor(hfdat$trt_ab)

# other variables I didn't transform: dthhosp,dhfu, cechfcfu, cechfcmp, peakvo3,
#peakvo12, death, deathfu, newid, trt_ab))

#save new database
#write.csv(hfdat, file = "hfaction1.csv")
#saveRDS(hfdat, file = "hfaction1r.rds")

#database with only complete variables, factored
library("dplyr")
hfactionfinal<-data.frame(select(hfdat, f.age:f.vevco2t, dthhosp, dhfu, 
cechfcfu, cechfcmp, death, deathfu, newid, trt_ab))

hfacomp<-data.frame(hfactionfinal[complete.cases(hfactionfinal),])

#write.csv(hfacomp, file = "hfactioncomp.csv")
#saveRDS(hfacomp, file = "hfactioncomp.rds")

###ignore this stuff - just playing with rpart for now###
hfdat$hfstat<-factor(hfdat$dthhosp, levels=0:1, labels=c("Adverse", "Non Adverse"))
hffit<-rpart(hfdat$dthhosp ~ f.age + f.bmi+ f.hrq+f.sex+f.sbpq, data=hfdat, control=rpart.control(minsplit=2, minbucket=3, cp=0.001))

plot(hffit, uniform=TRUE, 
    main="Classification Tree for HF-ACTION")
text(hffit, use.n=TRUE, all=FALSE, cex=.5)

> progstat <- factor(stagec$pgstat, levels = 0:1, labels = c("No", "Prog"))
> cfit <- rpart(progstat ~ age + eet + g2 + grade + gleason + ploidy,
                data = stagec, method = 'class')
                
#new database with new variables
#hfdatslim<-with(hfdat, data.frame(variables))

