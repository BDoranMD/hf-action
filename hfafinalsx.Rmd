
---
title: "lcaoutcomes expanded cohort - class generation"
author: "Bethany Doran"
date: "5/8/2018"
output:
  word_document: default
  html_document: default
---
#rm(list=ls())

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#read in data
hfa <- readRDS("~/hf-action/hfaction1r.rds")

#Chris's work
#install_bitbucket('hfclinicaldata/heart_analysis_utils')
library(heart.analysis.utils)
library(poLCA)
library(LCAvarsel)

```

```{r}
#create new variable with KCCQ Overall Score
hfa$kccqq<-NA
hfa$kccqq[hfa$KCCQOSB>=75]<-1
hfa$kccqq[hfa$KCCQSB>=50 & hfa$KCCQSB<75]<-2
hfa$kccqq[hfa$KCCQOSB<50]<-3

```

```{r}

###full data with cholesterol variables - hfa1

#make a dataframe that includes the cholesterol variables
hfa1<-with(hfa, data.frame(f.age, f.bmi, f.cabg, f.sex, f.sbpq, f.nyhaq, f.mi, f.ckd, f.copd, f.htn, f.diabetes,f.race, f.pci, f.vevco2t, f.rer, f.cpxdurq, f.peakvo2t, death, deathfu, hf, dhfu, newid, kccqq))
  
#make a dataframe with complete cases
hfa1c<-data.frame(hfa1[complete.cases(hfa1),])

#make a vector that contains variables above for the lca, complete and non complete

hfa1v<-with (hfa1, c(f.age, f.bmi, f.cabg, f.sex, f.sbpq, f.nyhaq, f.mi, f.ckd, f.copd, f.htn, f.diabetes,f.race, f.pci, f.vevco2t, f.rer, f.cpxdurq, f.peakvo2t))

hfa1vc<-with (hfa1c, c(f.age, f.bmi, f.cabg, f.sex, f.sbpq, f.nyhaq, f.mi, f.ckd, f.copd, f.htn, f.diabetes,f.race, f.pci, f.vevco2t, f.rer, f.cpxdurq, f.peakvo2t))

#dataframe with variables needed for LCA varsel
hfa1var<-with(hfa1c, data.frame(f.age, f.bmi, f.cabg, f.sex, f.sbpq, f.nyhaq, f.mi, f.ckd, f.copd, f.htn, f.diabetes,f.race, f.pci, f.vevco2t, f.cpxdurq, f.peakvo2t))
                           
#make an outcome variable for heart failure
hfa1$hf<- ifelse(hfa1$hf=="TRUE", 1,0 )
hfa1c$hf<- ifelse(hfa1c$hf=="TRUE", 1,0 )
```

```{r LCA varsel variable selection}
hfa1varsel <- LCAvarsel(hfa1var, G=3, parallel = TRUE)
list(hfa1varsel)


```

```{r random forest plots variable selection}
set.seed(1)
library(caret)
fit <- randomForest(x=hfa1c$hfa1var, y=hfa1c$death, ntree=400, mtry=8, importance=TRUE, do.trace=50)
library("caret", lib.loc="~/R/win-library/3.4")
(VI_F=importance(fit))
varImpPlot(fit,type=2)

```


```{r polca}

#poLCA iterative modeling
#all variables incl incomplete although varsel to select variables does not include incomplete - we will have to figure out what to do with the rer (whether to include or stratify by those over rer 1.1)

#just for ones selected from the LCA varsel
lca2_var<- with(hfa1c, cbind("f.age","f.cabg","f.ckd","f.race","f.vevco2t", "f.peakvo2t"  ))
lca2_formula        <- formula(paste("cbind(",paste(lca2_var, collapse=","),")~hfa1c$kccqq"))
lca2h_formula <- formula(  paste(  "predclass~(", paste(lca2_var, collapse="+"), ")")  )

polcahfa2 <- poLCA_iterate(f=lca2_formula, dat=hfa1c, maxclass=7, reps=18 )

```

# LCA on chosen number of classes for analysis of variables: 5 (seems to correspond to both LCA varsel for model selection variables and then match to the poLCA)
These graphs show the amount of influence a particular value has in placing a patient into a particular class. 
```{r Make model, generate baseline characteristics table, echo=T, collapse=T, results='hide', message=F, warning=F, fig.width=20, fig.height=20 }
#for the variables that were selected from varsel only; for this we are using 5 classes due to the graphs above
this_lcamodel <- poLCA(f=lca2_formula , data=hfa1c, nclass=5, nrep=10, na.rm=FALSE, graphs=TRUE)
hfa1c$predclass <- this_lcamodel$predclass

pidmat <- cbind(1, c(1:10))
exb<-exp(pidmat %*% this_lcamodel$coeff)
matplot(c(1:10), (cbind(1, exb)/(1 + rowSums(exb))),
main = "Symptom burden as a function of class",
xlab = "Low symptom burden (1) to High (10)",
ylab = "Probability of Latent Class Membership",
ylim = c(0, 1), type = "l", lwd = 3, col = 1)
text(8, 0.70, "High KCCQ")
text(5.9, 0.40, " KCCQ")
text(5.4, 0.75, " KCCQ")
text(1.8, 0.65, " KCCQ")

saveRDS(this_lcamodel, "lcamodel1sx.csv")


```

# LCA Results - The percentages in this table are the red bars in the graphs above. This graph adds the actual patient counts.

```{r results, echo=T, collapse=F, warning=F, results='asis'}
#tables for 1st cohort
Hmisc:::summary.formula(lca1h_formula, data=hfa1, FUN=table, method='reverse', test=T)

#tables for 2nd cohort
Hmisc:::summary.formula(lca1hc_formula, data=hfa1c, FUN=table, method='reverse', test=T)

```

# Boxplot 
#need help with this; is there a way to show more boxes?
Some classes fit better than others. Given a choice of a number of classes (often 5 or 6), the boxplots show how well each class fits the data.
```{r boxplot,  echo=T, collapse=T, message=F, warning=F } 
#needed to recode because something was happening with merge
poLCAfulldat <- function (df1, dat) {
  df <- cbind(df1$posterior,cbind(df1$predclass))
  endcol <- ncol(df)
  modal_prob <-vector()
  colnames(df)[endcol] <- 'modal_class'
  for (h in 1:(endcol-1)){
    colnames(df)[h] <- paste("Class",h,sep="_")
  }
  for (i in 1:nrow(df)) {
    thismodalclass <- df[i,endcol]
    modal_prob <- c(modal_prob,df[i,thismodalclass])  
  }
  df <- cbind(df,cbind(modal_prob))
  x <- cbind(dat,df)
  return(x)
}

hdcp_data_modal <- poLCA_find_modal_prob(polca_df=this_lcamodel, dat=hfa1c)

```

#save final data

```{r}
#write.csv(hdcp_data_modal, file = "hfactionclassesx.csv")
#saveRDS(hdcp_data_modal, file = "hfactionclassesx.rds")

```

