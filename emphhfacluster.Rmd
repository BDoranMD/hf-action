---
title: "Analysis and tables"
author: "B Doran"
date: "July 30, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#load the data
hfall <- readRDS("~/hf-action/emphhfa.Rds")
```


```{r cars}

library(data.table)
library(base)
library(tableone)
library(heart.analysis.utils)

```

```{r exercise variable creation}


#six minute walk
hfall$sixmwlkdq<-NA
hfall$sixmwlkdq[hfall$sixmwlkd<=300]<-4
hfall$sixmwlkdq[hfall$sixmwlkd>300 & hfall$sixmwlka<=372]<-3
hfall$sixmwlkdq[hfall$sixmwlkd>372 & hfall$sixmwlkd<=436]<-2
hfall$sixmwlkdq[hfall$sixmwlkd>436]<-1

hfall$f.sixmwlkdq<-factor(hfall$sixmwlkdq)

#peakvo2 - beta blocker vs. non beta blocker ?will have to look at how cpex was done to determine if they were on one vs. not
#if on b-blocker; <12 mL/kg/min pred
hfall$peakvo2t<-NA
hfall$peakvo2t[hfall$peakvo2>=20]<-1
hfall$peakvo2t[hfall$peakvo2<20 & hfall$peakvo2>=14 & hfall$betab==0]<-2
hfall$peakvo2t[hfall$peakvo2<20 & hfall$peakvo2>=12 & hfall$betab==1]<-2
hfall$peakvo2t[hfall$peakvo2<14 & hfall$betab==0]<-3
hfall$peakvo2t[hfall$peakvo2<12 & hfall$betab==1]<-3

hfall$f.peakvo2t<-factor(hfall$peakvo2t)

#vevco2
hfall$vevco2t<-NA
hfall$vevco2t[hfall$vevco2<30]<-1
hfall$vevco2t[hfall$vevco2>=30 & hfall$vevco2<36]<-2
hfall$vevco2t[hfall$vevco2>=36]<-3

hfall$f.vevco2t <-factor(hfall$vevco2t)

#cpx duration by quartile
hfall$cpxdurq<-NA
hfall$cpxdurq[hfall$cpxdur<7]<-4
hfall$cpxdurq[hfall$cpxdur>=7 & hfall$cpxdur<9.7]<-3
hfall$cpxdurq[hfall$cpxdur>=9.7 & hfall$cpxdur<12]<-2
hfall$cpxdurq[hfall$cpxdur>=12]<-1

hfall$f.cpxdurq<-factor(hfall$cpxdurq)

#adequate rer >=1.1
hfall$rer<-NA
hfall$rer[hfall$peakrer>=1.1]<-1
hfall$rer[hfall$peakrer<1.1]<-2

hfall$f.rer<-factor(hfall$rer)

```


```{r database}

varsToFactor<-c("agecat", "sex", "sbpcat", "hrcat", "bmicat", "lvefcat", "ckdcat", "anemia","htn", "dm", "smoke",
                "copd", "afib", "stroke", "angina", "revasc", "death", "cechfcmp", "trt_ab", "betab", "digoxin",
                "loopdiur", "acei", "arb")
hfall.f<-hfall
hfall.f[varsToFactor]<-lapply(hfall[varsToFactor], factor)

dput(names(hfall.f))

vars<-c("agecat", "sex", "sbpcat", "hrcat", "bmicat", "lvefcat", "ckdcat", "anemia","htn", "dm", "smoke",
                "copd", "afib", "stroke", "angina", "revasc", "death", "cechfcmp", "trt_ab", "betab", "digoxin",
                "loopdiur", "acei", "arb", "racec")

tableOne<-CreateTableOne(vars=vars, data=hfall.f,includeNA=TRUE)

```

```{r lca varsel dataframe}

hfav<-with(hfall, data.frame(agecat, sex, sbpcat, hrcat, bmicat, lvefcat, ckdcat, anemia, htn, dm, smoke,
           copd, afib, stroke, angina, revasc))

hfav.c<-data.frame(hfav[complete.cases(hfav),])

hfav1<-with(hfall, data.frame(agecat, sex, sbpcat, hrcat, bmicat, lvefcat, ckdcat, anemia, htn, dm, smoke,
           copd, afib, stroke, angina, revasc, racec, f.cpxdurq, f.peakvo2t, f.sixmwlkdq, f.vevco2t))

hfav1.c<-data.frame(hfav1[complete.cases(hfav1),])

hfav1.c <- hfav1.c[lapply(hfav1.c,length)>0]


```


```{r}

#with the variables from EMPHASIS
lca_var<- with(hfall.f, cbind("agecat", "sex", "sbpcat", "hrcat", "bmicat", "lvefcat", "ckdcat", "anemia",
                              "htn", "dm", "smoke", "copd", "afib", "revasc"))
lca_formula        <- formula(paste("cbind(",paste(lca_var, collapse=","),")~1"))
lcah_formula <- formula(  paste(  "predclass~(", paste(lca_var, collapse="+"), ")")  )

this_lcamodel <- poLCA(f=lca_formula , data=hfall.f, nclass=4, nrep=10, na.rm=FALSE, graphs=TRUE)

hdcp_data_modal <- poLCA_find_modal_prob(polca_df=this_lcamodel, dat=hfall.f)


boxplot(modal_prob~modal_class, dat=hdcp_data_modal)


```

```{r}

lca_var1<- with(hfall.f, cbind("agecat", "sex", "sbpcat", "hrcat", "bmicat", "lvefcat", "ckdcat", "anemia",
                              "htn", "dm", "smoke", "copd", "afib", "revasc", "racec", 
                              "f.peakvo2t", "f.vevco2t", "f.cpxdurq"))
lca_formula1        <- formula(paste("cbind(",paste(lca_var1, collapse=","),")~1"))
lcah_formula1 <- formula(  paste(  "predclass~(", paste(lca_var1, collapse="+"), ")")  )

this_lcamodel1 <- poLCA(f=lca_formula1 , data=hfall.f, nclass=4, nrep=10, na.rm=FALSE, graphs=TRUE)

hdcp_data_modal1 <- poLCA_find_modal_prob(polca_df=this_lcamodel1, dat=hfall.f)


boxplot(modal_prob~modal_class, dat=hdcp_data_modal1)


```
```{r LCA varsel}
library(LCAvarsel)
#hfLCA<-fitLCA(hfav.c, G=1:6, X=NULL)
#chose 4 as number of clusters

#LCAvarsel(hfav.c, G=4, X=NULL)
#varsel chose agecat bmicat ckdcat htn dm copd afib revasc

#hfLCA<-fitLCA(hfav1.c, G=1:6, X=NULL)
#chose 5 as number of clusters

#LCAvarsel(hfav1.c, G=5, X=NULL)
#LCA varsel: agecat ckdcat afib racec f.cpxdurq f.peakvo2t f.sixmwlkdq f.vevco2t 

```

```{r}

lca_var2<- with(hfall.f, cbind("agecat", "ckdcat", "afib", "racec", 
                              "f.peakvo2t", "f.sixmwlkdq", "f.cpxdurq"))
lca_formula2        <- formula(paste("cbind(",paste(lca_var2, collapse=","),")~1"))
lcah_formula2 <- formula(  paste(  "predclass~(", paste(lca_var2, collapse="+"), ")")  )

this_lcamodel2 <- poLCA(f=lca_formula2 , data=hfall.f, nclass=4, nrep=10, na.rm=FALSE, graphs=TRUE)

hdcp_data_modal2 <- poLCA_find_modal_prob(polca_df=this_lcamodel2, dat=hfall.f)

boxplot(modal_prob~modal_class, dat=hdcp_data_modal2)

```

```{r cox PH modeling}

library("survival")
library("survminer")

#cox PH model with EMPHASIS variables

this_lcamodel$class<-factor(this_lcamodel$predclass)

hfa1.surv <- survfit(Surv(hfall$deathfu, hfall$death) ~ this_lcamodel$class)

plot(hfa1.surv, lty = 1:4)

cox<-coxph(Surv(hfall$deathfu, hfall$death) ~ factor(this_lcamodel$class))


f.classii <- relevel(this_lcamodel$class, ref = 2)
hfacox1<-coxph(Surv(hfall$deathfu, hfall$death) ~ factor(f.classii))
hfacox1

#

this_lcamodel$class<-factor(this_lcamodel$predclass)

hfa1.surv <- survfit(Surv(hfall$deathfu, hfall$death) ~ this_lcamodel$class)

plot(hfa1.surv, lty = 1:4)

hfall$class1<-this_lcamodel$class

```
```{r cox PH for exercise}


#cox PH model with exercise variables

this_lcamodel1$class<-factor(this_lcamodel1$predclass)

hfa1.surv1 <- survfit(Surv(hfall$deathfu, hfall$death) ~ this_lcamodel1$class)

plot(hfa1.surv1, lty = 1:4)

cox1<-coxph(Surv(hfall$deathfu, hfall$death) ~ factor(this_lcamodel1$class))


hfall$class2<-this_lcamodel1$class

#cox PH model with limited variables

this_lcamodel2$class<-factor(this_lcamodel2$predclass)

hfa1.surv2 <- survfit(Surv(hfall$deathfu, hfall$death) ~ this_lcamodel2$class)

plot(hfa1.surv2, lty = 1:4)

cox2<-coxph(Surv(hfall$deathfu, hfall$death) ~ factor(this_lcamodel2$class))

hfall$class3<-this_lcamodel2$class
```

```{r}
library(data.table)
library(base)
library(Hmisc)
library(tangram)

hfatab<-data.table(hfall)
setkey(hfatab, class1)

tablex<-summary(class1~death+cechfcmp+agecat+sex+sbpcat+hrcat+bmicat+lvefcat+ckdcat+anemia+hyperten+dm+smoke+copd+afib+stroke+angina+revasc+
                  acei+betab+digoxin+loopdiur,data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

table1<-summary(class1~death+acei+afib+age+anemia+angina+arb+basehr+beckb+betab+bmicat+revasc+cechfcmp+ckdcat+copd+corelvef+
                  cpxdur+diabetes+digoxin+egfr+htn+KCCQOSB+KCCQQOLB+KCCQTSB+loopdiur+nyhaclbn+peakvo2+racec+sbp+sex+sixmwlka+sixmwlkd+smoke+stroke+trt_ab+vevco2, data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

table2<-html5(tangram(factor(class1)~death::Categorical+cechfcmp::Categorical+agecat::Categorical+sex::Categorical+sbpcat::Categorical+hrcat::Categorical+bmicat::Categorical+lvefcat::Categorical+ckdcat::Categorical+anemia::Categorical+hyperten::Categorical+dm::Categorical+smoke::Categorical+afib::Categorical+stroke::Categorical+angina::Categorical+revasc::Categorical+acei::Categorical+betab::Categorical+digoxin::Categorical+loopdiur::Categorical, data=hfatab, FUN=table, method="reverse", overall=T, test=T, digits=3, pctdig=1, prmsd=T, prtest="P"))            
table2


```


```{r}

setkey(hfatab, class2)

tablex2<-summary(class2~death+cechfcmp+agecat+sex+sbpcat+hrcat+bmicat+lvefcat+ckdcat+anemia+hyperten+dm+smoke+copd+afib+stroke+angina+revasc+
                  acei+betab+digoxin+loopdiur,data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

table12<-summary(class2~death+acei+afib+age+anemia+angina+arb+basehr+beckb+betab+bmicat+revasc+cechfcmp+ckdcat+copd+corelvef+
                  cpxdur+diabetes+digoxin+egfr+htn+KCCQOSB+KCCQQOLB+KCCQTSB+loopdiur+nyhaclbn+peakvo2+racec+sbp+sex+sixmwlka+sixmwlkd+smoke+stroke+trt_ab+vevco2, data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

tablex2

table12

table22<-html5(tangram(factor(class2)~death::Categorical+cechfcmp::Categorical+agecat::Categorical+sex::Categorical+sbpcat::Categorical+hrcat::Categorical+bmicat::Categorical+lvefcat::Categorical+ckdcat::Categorical+anemia::Categorical+hyperten::Categorical+dm::Categorical+smoke::Categorical+afib::Categorical+stroke::Categorical+angina::Categorical+revasc::Categorical+acei::Categorical+betab::Categorical+digoxin::Categorical+loopdiur::Categorical, data=hfatab, FUN=table, method="reverse", overall=T, test=T, digits=3, pctdig=1, prmsd=T, prtest="P"))          
table22

```
```{r}

setkey(hfatab, class3)

tablex3<-summary(class3~death+cechfcmp+agecat+sex+sbpcat+hrcat+bmicat+lvefcat+ckdcat+anemia+hyperten+dm+smoke+copd+afib+stroke+angina+revasc+
                  acei+betab+digoxin+loopdiur,data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

table13<-summary(class3~death+acei+afib+age+anemia+angina+arb+basehr+beckb+betab+bmicat+revasc+cechfcmp+ckdcat+copd+corelvef+
                  cpxdur+diabetes+digoxin+egfr+htn+KCCQOSB+KCCQQOLB+KCCQTSB+loopdiur+nyhaclbn+peakvo2+racec+sbp+sex+sixmwlka+sixmwlkd+smoke+stroke+trt_ab+vevco2, data=hfatab,FUN=table,method="reverse",overall=T,test=T,digits=3,pctdig=1,prmsd=T,prtest="P")

tablex3

table13

table33<-html5(tangram(factor(class3)~death::Categorical+cechfcmp::Categorical+agecat::Categorical+sex::Categorical+sbpcat::Categorical+hrcat::Categorical+bmicat::Categorical+lvefcat::Categorical+ckdcat::Categorical+anemia::Categorical+hyperten::Categorical+dm::Categorical+smoke::Categorical+afib::Categorical+stroke::Categorical+angina::Categorical+revasc::Categorical+acei::Categorical+betab::Categorical+digoxin::Categorical+loopdiur::Categorical, data=hfatab, FUN=table, method="reverse", overall=T, test=T, digits=3, pctdig=1, prmsd=T, prtest="P"))
                      
table33

```



